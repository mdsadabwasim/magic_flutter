import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:magic_flutter/magic_flutter.dart';

void main() {
  const MethodChannel channel = MethodChannel('magic');
  final publisherKey = "xyz";
  final email = "abc@g.com";
  final fromAddress = "0x123";
  final toAddress = "0x111";
  final balance = "1.0";
  // final didTokenMap = {
  //   "id": 1,
  //   "jsonRpc": "jsonRpc",
  //   "rawResponse": "xyz",
  //   "result": "result",
  // };
  final metaDataMap = {
    "email": email,
    "publicAddress": 'publicAddress',
    "issuer": "issuer",
  };

  TestWidgetsFlutterBinding.ensureInitialized();

  group("Magic Flutter Plugin test", () {
    setUp(() {
      channel.setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == "initializeMagic") {
          if (publisherKey == publisherKey)
            return true;
          throw PlatformException(code: "FAILED");
        } else if (methodCall.method == "loginWithMagicLink") {
         return '1';
        } else if (methodCall.method == "isLoggedIn")
          return true;
        else if (methodCall.method == "logout")
          return true;
        else if (methodCall.method == "updateEmail") {
          if (methodCall.arguments == email) return true;
          throw PlatformException(code: "Failed");
        } else if (methodCall.method == "getMetaData")
          return metaDataMap;
        else if (methodCall.method == "getUserBalance") {
          if (methodCall.arguments == fromAddress) return balance;
          throw PlatformException(code: "Failed");
        } else if (methodCall.method == "signTypedData") {
          return 'Signed data';
        } else if (methodCall.method == "sendTransaction") {
         return 'Transaction successfull';
        }
      });
    });

    tearDown(() {
      channel.setMockMethodCallHandler(null);
    });

    test('initializeMagic', () async {
      expect(await Magic.initializeMagic(publisherKey: publisherKey), true);
    });

    test('loginWithMagicLink', () async {
      final result = await Magic.loginWithMagicLink(email: email,showLoadingUI: true);
      expect(result, isA<String>());
      // final didToken = DidToken.fromJson(didTokenMap);

      // expect(result.id, didToken.id);
      // expect(result.result, didToken.result);
      // expect(result.rawResponse, didToken.rawResponse);
      // expect(result.jsonRpc, didToken.jsonRpc);
    });

    test('isLoggedIn', () async {
      expect(await Magic.isLoggedIn(), true);
    });

    test('logout', () async {
      expect(await Magic.logout(), true);
    });

    test('updateEmail', () async {
      expect(await Magic.updateEmail(email: email), true);
    });

    test('getMetaData', () async {
      final result = await Magic.getMetaData();

      final metaData = GetMetaDataResponse.fromJson(metaDataMap);

      expect(result.email, metaData.email);
      expect(result.issuer, metaData.issuer);
      expect(result.publicAddress, result.publicAddress);
    });

    test('getUserBalance', () async {
      final result = await Magic.getUserBalance(accountAddress: fromAddress);
      expect(result, '1.0');
    });


    test('send Transaction', () async {
      final result = await Magic.sendTransaction(
          from: fromAddress, to: toAddress, amount: balance);
      expect(result, 'Transaction successfull');
    });
  });
}
