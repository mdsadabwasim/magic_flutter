package link.magic.flutter

import android.app.Activity
import android.os.Handler
import android.os.Looper
import androidx.annotation.NonNull
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import link.magic.android.Magic
import link.magic.android.core.urlBuilder.CustomNodeConfiguration
import link.magic.android.modules.auth.requestConfiguration.LoginWithMagicLinkConfiguration
import link.magic.android.modules.auth.response.DIDToken
import link.magic.android.modules.user.requestConfiguration.UpdateEmailConfiguration
import link.magic.android.modules.user.response.GetMetadataResponse
import link.magic.android.modules.user.response.IsLoggedInResponse
import link.magic.android.modules.user.response.LogoutResponse
import link.magic.android.modules.user.response.UpdateEmailResponse
import link.magic.android.modules.web3j.contract.MagicTxnManager
import link.magic.android.modules.web3j.signTypedData.request.EIP712TypedDataLegacyFields
import link.magic.android.modules.web3j.signTypedData.response.SignTypedData
import org.web3j.protocol.Web3j
import org.web3j.protocol.core.DefaultBlockParameterName
import org.web3j.protocol.core.methods.request.Transaction.createEtherTransaction
import org.web3j.protocol.core.methods.response.*
import org.web3j.tx.gas.StaticGasProvider
import org.web3j.utils.Convert
import java.math.BigDecimal
import java.math.BigInteger

/** MagicFlutterPlugin */
class MagicFlutterPlugin : FlutterPlugin, MethodCallHandler, ActivityAware {

    // Error constants
    private val errorCode: String = "FAILED"
    private val loginErrorMessage: String = "Failed to login"
    private val commonErrorDetail: String = "Something went wrong"
    private val isLoggedInErrorMessage: String = "Failed to check user status"
    private val logOutErrorMessage: String = "Failed to logout"
    private val updateEmailErrorMessage: String = "Failed to update E-mail"
    private val fetchMetaDataErrorMessage: String = "Failed to fetch user meta-data"
    private val fetchUserAccountDataErrorMessage: String = "Failed to fetch user account data"
    private val fetchCurrentGasPriceErrorMessage: String = "Failed to fetch current gas price"
    private val fetchUserContractBalanceErrorMessage: String =
        "Failed to fetch user account balance from contract"
    private val contractTransactionErrorMessage: String = "Failed to do transaction from contract"
    private val fetchUserBalanceDataErrorMessage: String = "Failed to fetch user balance data"
    private val failedToCreateSignedMessage: String = "Failed to create signed message"
    private val failedToSendTransactionErrorMessage: String = "Failed to send transaction"

    private lateinit var channel: MethodChannel
    private var activity: Activity? = null
    private var magicSDK: Magic? = null
    private lateinit var web3j: Web3j
    private lateinit var account: String


    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "magic")
        channel.setMethodCallHandler(this)
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "initializeMagic" -> initializeMagicSDK(call = call, result = result)
            "loginWithMagicLink" -> loginWithMagicLink(call = call, result = result)
            "isLoggedIn" -> isLoggedIn(result = result)
            "logout" -> logout(result = result)
            "updateEmail" -> updateEmail(call = call, result = result)
            "getMetaData" -> getMetaData(result = result)
            "getAccount" -> getAccount(result = result)
            "sendTransaction" -> sendTransaction(call = call, result = result)
            "getUserBalance" -> getUserBalance(call = call, result = result)
            "getContractCurrentGasPrice" -> getContractCurrentGasPrice(result = result)
            "getDynamicContractUserBalance" -> getDynamicContractUserBalance(
                call = call,
                result = result
            )
            "sendDynamicContractTransaction" -> sendDynamicContractTransaction(
                call = call,
                result = result
            )
            "sign" -> sign(call = call, result = result)
            "signTypedData" -> signTypedData(call = call, result = result)
            "signTypedDataLegacy" -> signTypedDataLegacy(call = call, result = result)
            else -> result.notImplemented()
        }
    }

    private fun initializeMagicSDK(call: MethodCall, result: Result) {
        if (activity != null) {
            if (call.argument<String>("rpcUrl") != null && call.argument<String>("chainId") != null) {
                magicSDK = Magic(call.argument<String>("publisherKey")!!,
                    CustomNodeConfiguration(
                        call.argument<String>("rpcUrl")!!, call.argument<String>("chainId")
                    )
                )
                web3j = Web3j.build(magicSDK!!.rpcProvider)
            } else {
                magicSDK = Magic(call.argument<String>("publisherKey")!!)
                web3j = Web3j.build(magicSDK!!.rpcProvider)
            }

            magicSDK!!.startRelayer(activity as Activity)
            result.success(true)
        } else {
            result.success(false)
        }
    }

    /// After user is successfully authenticated, call to get user information
    private fun getAccount(result: Result) {
        try {
            val accounts = web3j.ethAccounts().sendAsync()

            accounts.whenComplete { accResponse: EthAccounts?, error: Throwable? ->
                if (error != null) {
                    println("Error: " + error.localizedMessage)
                    sendError(fetchUserAccountDataErrorMessage, error.message, result)
                }
                if (accResponse != null && !accResponse.hasError()) {
                    account = accResponse.accounts[0]
                    println("Your address is $account")
                    sendResult(account, result)
                }
            }
        } catch (e: Exception) {
            println("Error: " + e.localizedMessage)
            sendSDKNotInitializeError(result)
        }
    }

    private fun getContractCurrentGasPrice(result: Result) {
        try {
            val ethCall = web3j.ethGasPrice().sendAsync()
            ethCall.whenComplete { balance: EthGasPrice?, error: Throwable? ->
                if (error != null) {
                    println("Error: " + error.localizedMessage)
                    sendError(fetchCurrentGasPriceErrorMessage, error.message, result)
                } else if (balance == null) {
                    println("Error: Failed to get current gas price")
                    sendError(
                        fetchCurrentGasPriceErrorMessage,
                        "Failed to get current gas price",
                        result
                    )
                }
                if (balance != null) {
                    val value = balance.gasPrice.toString()
                    println("Current gas price $value")
                    sendResult(value, result)
                }
            }
        } catch (e: Exception) {
            println(e.toString())
            sendSDKNotInitializeError(result)
        }
    }

    private fun getDynamicContractUserBalance(call: MethodCall, result: Result) {
        try {
            val price = BigInteger.valueOf(22000000000L)
            val limit = BigInteger.valueOf(4300000)
            val gasProvider = StaticGasProvider(price, limit)

            val contract = GenericContract.load(
                call.argument<String>("byteCode"),
                call.argument<String>("contractAddress"),
                web3j,
                MagicTxnManager(web3j, call.argument<String>("accountAddress")!!),
                gasProvider
            )
            val ethCall =
                contract.balanceOf(call.argument<String>("accountAddress")!!).sendAsync()
            ethCall.whenComplete { balance: BigInteger?, error: Throwable? ->
                if (error != null) {
                    println("Error: " + error.localizedMessage)
                    sendError(fetchUserContractBalanceErrorMessage, error.message, result)
                } else if (balance == null) {
                    println("Error: Failed to fetch balance")
                    sendError(fetchUserContractBalanceErrorMessage, "Failed to get balance", result)
                }
                if (balance != null) {
                    val newValue: BigDecimal =
                        Convert.fromWei(balance.toString(), Convert.Unit.ETHER)
                    println("Token balance $newValue")
                    sendResult(newValue.toString(), result)
                }
            }
        } catch (e: Exception) {
            println(e.toString())
            sendSDKNotInitializeError(result)
        }
    }

    private fun sendDynamicContractTransaction(call: MethodCall, result: Result) {
        try {
            val price = BigInteger(call.argument<String>("gasPrice")!!)
            val limit = BigInteger(call.argument<String>("gasLimit")!!)
            val gasProvider = StaticGasProvider(price, limit)
            val value: BigInteger =
                Convert.toWei(call.argument<String>("amount"), Convert.Unit.ETHER)
                    .toBigInteger()

            val contract = GenericContract.load(
                call.argument<String>("byteCode"),
                call.argument<String>("contractAddress"),
                web3j,
                MagicTxnManager(web3j, call.argument<String>("from")!!),
                gasProvider
            )
            val transactionCall = contract.transfer(
                call.argument<String>("to")!!,
                value
            ).sendAsync()
            transactionCall.whenComplete { receipt: TransactionReceipt?, error: Throwable? ->
                if (error != null) {
                    println("Error: " + error.localizedMessage)
                    sendError(contractTransactionErrorMessage, error.message, result)
                }
                if (receipt != null) {
                    println("Transaction hash ${receipt.transactionHash}")
                    sendResult(receipt.transactionHash, result)
                }
            }
        } catch (e: Exception) {
            println(e.toString())
            sendSDKNotInitializeError(result)
        }
    }

    private fun getUserBalance(call: MethodCall, result: Result) {
        try {
            val balanceFuture = web3j.ethGetBalance(
                call.arguments as String,
                DefaultBlockParameterName.LATEST
            )
                .sendAsync()
            balanceFuture.whenComplete { balance: EthGetBalance?, error: Throwable? ->
                if (error != null) {
                    println("Error: " + error.localizedMessage)
                    sendError(fetchUserBalanceDataErrorMessage, error.message, result)
                }
                if (balance != null && !balance.hasError()) {
                    val wei: BigInteger = balance.balance
                    val newValue: BigDecimal = Convert.fromWei(wei.toString(), Convert.Unit.ETHER)
                    println("Your balance is $newValue")
                    sendResult(newValue.toString(), result)
                }
            }
        } catch (e: Exception) {
            println("Error: " + e.localizedMessage)
            sendSDKNotInitializeError(result)
        }
    }

    private fun sendTransaction(call: MethodCall, result: Result) {
        //Getting current nonce value
        lateinit var nonce: BigInteger
        val nonceFuture = web3j.ethGetTransactionCount(
            call.argument<String>("from"),
            DefaultBlockParameterName.PENDING
        ).sendAsync()
        nonceFuture.whenComplete { nonceValue: EthGetTransactionCount?, error: Throwable? ->

            if (nonceValue != null && !nonceValue.hasError()) {
                nonce = nonceValue.transactionCount
                println("Nonce value: $nonce")
            } else {
                nonce = BigInteger.ONE
                println("Error: " + error!!.localizedMessage)
            }

            //Once we got the nonce then sending transaction
            try {

                val value: BigInteger =
                    Convert.toWei(call.argument<String>("amount"), Convert.Unit.ETHER)
                        .toBigInteger()
                val transaction = createEtherTransaction(
                    call.argument<String>("from"),
                    nonce,
                    BigInteger(call.argument<String>("gasPrice")!!),
                    BigInteger(call.argument<String>("gasLimit")!!),
                    call.argument<String>("to"),
                    value
                )
                val receiptFuture = web3j.ethSendTransaction(transaction).sendAsync()
                receiptFuture.whenComplete { receipt: EthSendTransaction?, sendError: Throwable? ->
                    if (sendError != null) {
                        println("Error: " + sendError.localizedMessage)
                        sendError(failedToSendTransactionErrorMessage, sendError.message, result)
                    }
                    if (receipt != null && !receipt.hasError()) {
                        println("Transaction complete: " + receipt.transactionHash)
                        sendResult(receipt.transactionHash, result)
                    } else {
                        println("Error: " + receipt!!.error.message)
                        sendError(
                            failedToSendTransactionErrorMessage,
                            receipt.error.message,
                            result
                        )
                    }
                }

            } catch (e: Exception) {
                println("Error: " + e.localizedMessage)
                sendSDKNotInitializeError(result)
            }
        }
    }

    private fun sign(call: MethodCall, result: Result) {
        try {
            val message = call.argument<String>("message")
            val accountAddress = call.argument<String>("fromAccount")
            val signedData = web3j.ethSign(accountAddress, message)
                .sendAsync()
            signedData.whenComplete { signMessage: EthSign?, error: Throwable? ->
                if (error != null) {
                    println("Error: " + error.localizedMessage)
                    sendError(failedToCreateSignedMessage, error.message, result)
                }
                if (signMessage != null && !signMessage.hasError()) {
                    val trxHash = signMessage.signature
                    println("Your hash is $trxHash")
                    sendResult(trxHash.toString(), result)
                }
            }
        } catch (e: Exception) {
            println("Error: " + e.localizedMessage)
            sendSDKNotInitializeError(result)
        }
    }

    private fun signTypedDataLegacy(call: MethodCall, result: Result) {
        try {
            val type = call.argument<String>("type")
            val name = call.argument<String>("name")
            val value = call.argument<String>("value")
            val accountAddress = call.argument<String>("fromAccount")
            val list = listOf(EIP712TypedDataLegacyFields(type, name, value))
            val signature =
                magicSDK!!.web3jSigExt.signTypedDataLegacy(accountAddress, list).sendAsync()
            signature.whenComplete { signMessage: SignTypedData?, error: Throwable? ->
                if (error != null) {
                    println("Error: " + error.localizedMessage)
                    sendError(failedToCreateSignedMessage, error.message, result)
                }
                if (signMessage != null && !signMessage.hasError()) {
                    val trxHash = signMessage.result
                    println("Your hash is $trxHash")
                    sendResult(trxHash.toString(), result)
                }
            }
        } catch (e: Exception) {
            println("Error: " + e.localizedMessage)
            sendSDKNotInitializeError(result)
        }
    }

    private fun signTypedData(call: MethodCall, result: Result) {
        try {
            val jsonString = call.argument<String>("json")
            val accountAddress = call.argument<String>("fromAccount")
            val signatureFuture =
                magicSDK!!.web3jSigExt.signTypedDataV4(accountAddress, jsonString!!).sendAsync()
            signatureFuture.whenComplete { signature: SignTypedData?, error: Throwable? ->
                if (error != null) {
                    println("Error: " + error.localizedMessage)
                    sendError(failedToCreateSignedMessage, error.message, result)
                }
                if (signature != null && !signature.hasError()) {
                    val trxHash = signature.result
                    println("Your hash is $trxHash")
                    sendResult(trxHash.toString(), result)
                }
            }
        } catch (e: Exception) {
            println("Error: " + e.localizedMessage)
            sendSDKNotInitializeError(result)
        }
    }

    private fun getMetaData(result: Result) {
        if (magicSDK != null) {
            val completableFuture = magicSDK!!.user.getMetadata()

            completableFuture.whenComplete { response: GetMetadataResponse?, error: Throwable? ->
                if (response != null && !response.hasError()) {
                    val responseMap = mapOf(
                        "email" to response.result.email,
                        "issuer" to response.result.issuer,
                        "publicAddress" to response.result.publicAddress
                    )
                    account = response.result.publicAddress!!
                    sendResult(responseMap, result)
                } else if (error != null) {
                    sendError(fetchMetaDataErrorMessage, error.message, result)
                } else {
                    sendError(fetchMetaDataErrorMessage, commonErrorDetail, result)
                }
            }
        } else {
            sendSDKNotInitializeError(result)
        }
    }

    private fun updateEmail(call: MethodCall, result: Result) {
        if (magicSDK != null) {
            val completableFuture =
                magicSDK!!.user.updateEmail(UpdateEmailConfiguration(email = call.arguments as String))

            completableFuture.whenComplete { response: UpdateEmailResponse?, error: Throwable? ->
                when {
                    response != null && !response.hasError() -> {
                        sendResult<Boolean>(response.result, result)
                    }
                    error != null -> {
                        sendError(updateEmailErrorMessage, error.message, result)
                    }
                    else -> {
                        sendResult(false, result)
                    }
                }
            }
        } else {
            sendSDKNotInitializeError(result)
        }
    }

    private fun logout(result: Result) {
        if (magicSDK != null) {
            val completableFuture = magicSDK!!.user.logout()

            completableFuture.whenComplete { response: LogoutResponse?, error: Throwable? ->
                if (error != null)
                    sendError(logOutErrorMessage, error.message, result)

                if (response != null && response.result) {
                    sendResult<Boolean>(response.result, result)
                }
            }

        } else {
            sendSDKNotInitializeError(result)
        }
    }

    private fun isLoggedIn(result: Result) {
        if (magicSDK != null) {
            val completableFuture = magicSDK!!.user.isLoggedIn()
            completableFuture.whenComplete { response: IsLoggedInResponse?, error: Throwable? ->
                if (error != null)
                    sendError(isLoggedInErrorMessage, error.message, result)

                if (response != null && response.result) {
                    sendResult<Boolean>(response.result, result)
                } else {
                    sendResult(false, result)
                }
            }
        } else {
            sendSDKNotInitializeError(result)
        }

    }

    private fun loginWithMagicLink(call: MethodCall, result: Result) {
        if (magicSDK != null) {
            val completableFuture =
                magicSDK!!.auth.loginWithMagicLink(
                    LoginWithMagicLinkConfiguration(
                        showUI = call.argument<Boolean>("showUI"),
                        email = call.argument<String>("email")!!
                    )
                )

            completableFuture.whenComplete { response: DIDToken?, error: Throwable? ->
                if (error != null)
                    sendError(loginErrorMessage, error.message, result)

                if (response != null && !response.hasError()) {
//                    val responseMap = mapOf(
//                        "id" to response.id,
//                        "result" to response.result,
//                        "jsonRpc" to response.jsonrpc,
//                        "rawResponse" to response.rawResponse
//                    )
//                    sendResult<Map<String, Any>>(responseMap, result)
                    sendResult(response.id.toString(), result)
                } else {
                    sendError(loginErrorMessage, commonErrorDetail, result)
                }
            }
        } else {
            sendSDKNotInitializeError(result)
        }
    }

    private fun sendSDKNotInitializeError(result: Result) {
        result.error(
            errorCode,
            "Magic SDK not initialized. Make sure, you have called Magic.initializeMagic",
            null
        )
    }

    private fun <T> sendResult(response: T, result: Result) {
        Handler(Looper.getMainLooper()).post {
            result.success(response)
        }
    }

    private fun sendError(errorMessage: String, errorDetails: String?, result: Result) {
        Handler(Looper.getMainLooper()).post {
            result.error(errorCode, errorMessage, errorDetails)
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }

    override fun onDetachedFromActivity() {
        activity = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        activity = binding.activity
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding.activity
    }

    override fun onDetachedFromActivityForConfigChanges() {
        activity = null
    }
}
