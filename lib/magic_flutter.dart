import 'dart:async';
import 'package:magic_flutter/src/index.dart';
export 'package:magic_flutter/src/index.dart';

/// The entry point for accessing Magic SDK.
class Magic {
  /// Initializes a new Magic SDK instance using the given [publisherKey],
  ///
  /// for custom node configuration provide chain id[String] and rpc url[String]
  ///
  /// On initializing successfully, it will return `true`.
  static Future<bool> initializeMagic(
      {required String publisherKey, String? chainID, String? rpcURL}) async {
    return await MagicPlatformInterface.instance.initializeMagic(
        publisherKey: publisherKey, chainId: chainID, rpcUrl: rpcURL);
  }

  /// Allows users to login with magic link.
  /// pass `email`, and `showLoadingUI` to true to show out of the box loading UI,
  /// Upon successful login, returns
  /// [Future] of [String].
  static Future<String> loginWithMagicLink(
      {required String email, required bool showLoadingUI}) async {
    return await MagicPlatformInterface.instance
        .loginWithMagicLink(email: email, showUI: showLoadingUI);
  }

  /// Returns [Future] of [bool], which denotes if user has already logged in, or not.
  static Future<bool> isLoggedIn() async {
    return await MagicPlatformInterface.instance.isLoggedIn();
  }

  /// Method to logout, if user is logged in. Returns [Future] of [bool] to denote,
  /// whether user has successfully logged out.
  static Future<bool> logout() async {
    return await MagicPlatformInterface.instance.logout();
  }

  /// Method to update email id of already logged in user. The method should
  /// be used if user is already logged int.
  static Future<bool> updateEmail({required String email}) async {
    return await MagicPlatformInterface.instance.updateEmail(email: email);
  }

  /// Method to get meta data of already logged in user. Returns
  /// [Future] of [GetMetaDataResponse] on success.
  static Future<GetMetaDataResponse> getMetaData() async {
    return await MagicPlatformInterface.instance.getMetaData();
  }

  /// Method to get user account data of already logged in user. Returns
  /// [Future] of [String] on success.
  static Future<String> getUserAccountData() async {
    return await MagicPlatformInterface.instance.getAccount();
  }

  /// Method to get user balance data of already logged in user. Returns
  /// [Future] of [String] on success.
  static Future<String> getUserBalance({required String accountAddress}) async {
    return await MagicPlatformInterface.instance
        .getUserBalance(accountAddress: accountAddress);
  }

  /// Method to get current gas price. Returns
  /// [Future] of [String] on success.
  static Future<String> getCurrentGasPrice() async {
    return await MagicPlatformInterface.instance.getCurrentGasPrice();
  }

  /// Method to send transaction.
  /// It requires from:[String] your account address, to:[String] account address you want the amount to send,
  /// amount:[String] Amount in BNB, `required for android` gasPrice: [String] gas price value,`required for android`  gasLimit: [String] gas limit value
  /// Returns [Future] of [String] with transaction hash on success.
  static Future<String> sendTransaction(
      {required String from,
      required String to,
      required String amount,
      String? gasPrice,
      String? gasLimit}) async {
    return await MagicPlatformInterface.instance.sendTransaction(
        fromAccount: from,
        toAccount: to,
        transactionAmount: amount,
        gasLimitAmount: gasLimit,
        gasPriceAmount: gasPrice);
  }

  /// Method to send transaction with custom contract.
  /// It requires from:[String] your account address,
  /// to:[String] account address you want the amount to send,
  /// `not required in android` contractABI:[String] your deployed contract abi,
  ///  /// `not required in ios` byteCode:[String] your deployed contract byteCode,
  /// contractAddress:[String] your deployed contract address
  /// amount:[String] Amount, gasPrice: [String] gas price value, gasLimit: [String] gas limit value
  /// Returns [Future] of [String] with transaction hash on success.
  static Future<String> sendDynamicContractTransaction(
      {required String from,
      required String to,
      required String amount,
      String? contractABI,
      String? byteCode,
      required String contractAddress,
      required String gasPrice,
      required String gasLimit}) async {
    return await MagicPlatformInterface.instance.sendDynamicContractTransaction(
        fromAccount: from,
        toAccount: to,
        contractABI: contractABI ?? "",
        byteCode: byteCode ?? "",
        contractAddress: contractAddress,
        transactionAmount: amount,
        gasLimitAmount: gasLimit,
        gasPriceAmount: gasPrice);
  }

  /// Method to get user balance with custom contract.
  /// It requires accountAddress:[String] user account address,
  /// `not required in android` contractABI:[String] your deployed contract abi,
  /// `not required in ios` byteCode:[String] your deployed contract byteCode,
  /// contractAddress:[String] your deployed contract address
  /// Returns [Future] of [String] on success.
  static Future<String> getDynamicContractUserBalance({
    required String accountAddress,
    String? contractABI,
    String? byteCode,
    required String contractAddress,
  }) async {
    return await MagicPlatformInterface.instance.getDynamicContractUserBalance(
        accountAddress: accountAddress,
        contractABI: contractABI ?? "",
        byteCode: byteCode ?? "",
        contractAddress: contractAddress);
  }

  /// Eth Sign
  static Future<String> sign({
    required String fromAccount,
    required String message,
  }) async {
    return await MagicPlatformInterface.instance
        .sign(fromAccount: fromAccount, message: message);
  }

  /// Sign Typed Data (EIP 712)
  static Future<String> signTypedDataLegacy({
    required String fromAccount,
    required String type,
    required String value,
    required String name,
  }) async {
    return await MagicPlatformInterface.instance.signTypedDataLegacy(
        fromAccount: fromAccount, type: type, name: name, value: value);
  }

  static Future<String> signTypedData({
    required String fromAccount,
    required String json,
  }) async {
    return await MagicPlatformInterface.instance
        .signTypedData(fromAccount: fromAccount, json: json);
  }
}
