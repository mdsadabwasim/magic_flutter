import 'package:magic_flutter/src/magic_method_channel.dart';
import 'package:magic_flutter/src/model/generate_id_token_model.dart';
import 'package:magic_flutter/src/model/get_id_token_model.dart';
import 'package:magic_flutter/src/model/get_metadata_response_model.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

abstract class MagicPlatformInterface extends PlatformInterface {
  MagicPlatformInterface() : super(token: _token);

  static MagicPlatformInterface _instance = MagicMethodChannel();

  static final Object _token = Object();

  static MagicPlatformInterface get instance => _instance;

  /// Platform-specific plugins should set this with their own platform-specific
  /// class that extends [MagicPlatformInterface] when they register themselves.
  static set instance(MagicPlatformInterface instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<bool> initializeMagic(
      {required String publisherKey, String? chainId, String? rpcUrl}) async {
    throw UnimplementedError();
  }

  Future<String> loginWithMagicLink(
      {required String email, required bool showUI}) async {
    throw UnimplementedError();
  }

  Future<bool> updateEmail({required String email}) async {
    throw UnimplementedError();
  }

  Future<GetIdTokenResponse> getIdToken() async {
    throw UnimplementedError();
  }

  Future<GenerateIdTokenResponse> generateIdToken() async {
    throw UnimplementedError();
  }

  Future<GetMetaDataResponse> getMetaData() async {
    throw UnimplementedError();
  }

  Future<String> sendTransaction(
      {required String fromAccount,
      required String toAccount,
      required String transactionAmount,
      String? gasPriceAmount,
      String? gasLimitAmount}) async {
    throw UnimplementedError();
  }

  Future<String> sendDynamicContractTransaction(
      {required String fromAccount,
      required String toAccount,
      required String transactionAmount,
      String? contractABI,
      String? byteCode,
      required String contractAddress,
      required String gasPriceAmount,
      required String gasLimitAmount}) async {
    throw UnimplementedError();
  }

  Future<String> getDynamicContractUserBalance({
    required String accountAddress,
    String? contractABI,
    String? byteCode,
    required String contractAddress,
  }) async {
    throw UnimplementedError();
  }

  Future<String> sign({
    required String fromAccount,
    required String message,
  }) async {
    throw UnimplementedError();
  }

  Future<String> signTypedDataLegacy({
    required String fromAccount,
    required String type,
    required String name,
    required String value,
  }) async {
    throw UnimplementedError();
  }

  Future<String> signTypedData(
      {required String fromAccount, required String json}) async {
    throw UnimplementedError();
  }

  Future<String> getAccount() async {
    throw UnimplementedError();
  }

  Future<String> getUserBalance({required String accountAddress}) async {
    throw UnimplementedError();
  }

  Future<String> getCurrentGasPrice() async {
    throw UnimplementedError();
  }

  Future<bool> isLoggedIn() async {
    throw UnimplementedError();
  }

  Future<bool> logout() async {
    throw UnimplementedError();
  }
}
