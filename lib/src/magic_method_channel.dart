import 'package:flutter/services.dart';
import 'package:magic_flutter/src/magic_platform_interface.dart';
import 'package:magic_flutter/src/model/generate_id_token_model.dart';
import 'package:magic_flutter/src/model/get_id_token_model.dart';
import 'package:magic_flutter/src/model/get_metadata_response_model.dart';

class MagicMethodChannel implements MagicPlatformInterface {
  static const MethodChannel _methodChannel = MethodChannel('magic');

  @override
  Future<GenerateIdTokenResponse> generateIdToken() {
    throw UnimplementedError();
  }

  @override
  Future<GetIdTokenResponse> getIdToken() {
    throw UnimplementedError();
  }

  @override
  Future<GetMetaDataResponse> getMetaData() async {
    return GetMetaDataResponse.fromJson(
        await _methodChannel.invokeMapMethod<String, dynamic>('getMetaData') ??
            <String, dynamic>{});
  }

  @override
  Future<bool> isLoggedIn() async {
    final result = await _methodChannel.invokeMethod("isLoggedIn") as bool;
    return result;
  }

  @override
  Future<String> loginWithMagicLink(
      {required String email, required bool showUI}) async {
    // final result = await _methodChannel.invokeMapMethod<String, dynamic>(
    //         'loginWithMagicLink', email) ??
    //     <String, dynamic>{};
    // return DidToken.fromJson(result);
    Map data = {"email": email, "showUI": showUI};
    final result =
        await _methodChannel.invokeMethod('loginWithMagicLink', data) as String;
    return result;
  }

  @override
  Future<bool> logout() async {
    final result = await _methodChannel.invokeMethod('logout') as bool;
    return result;
  }

  @override
  Future<bool> updateEmail({required String email}) async {
    final result =
        await _methodChannel.invokeMethod('updateEmail', email) as bool;
    return result;
  }

  @override
  Future<String> sendTransaction(
      {required String fromAccount,
      required String toAccount,
      required String transactionAmount,
      String? gasPriceAmount,
      String? gasLimitAmount}) async {
    Map data = {
      "from": fromAccount,
      "to": toAccount,
      "amount": transactionAmount,
      "gasPrice": gasPriceAmount,
      "gasLimit": gasLimitAmount
    };
    final result = await _methodChannel.invokeMethod('sendTransaction', data);
    return result;
  }

  @override
  Future<String> sendDynamicContractTransaction(
      {required String fromAccount,
      required String toAccount,
      required String transactionAmount,
      String? contractABI,
      String? byteCode,
      required String contractAddress,
      required String gasPriceAmount,
      required String gasLimitAmount}) async {
    Map data = {
      "from": fromAccount,
      "to": toAccount,
      "amount": transactionAmount,
      "contractABI": contractABI,
      "byteCode": byteCode,
      "contractAddress": contractAddress,
      "gasPrice": gasPriceAmount,
      "gasLimit": gasLimitAmount
    };
    final result = await _methodChannel.invokeMethod(
        'sendDynamicContractTransaction', data);
    return result;
  }

  @override
  Future<String> getDynamicContractUserBalance(
      {required String accountAddress,
      String? contractABI,
      String? byteCode,
      required String contractAddress}) async {
    Map data = {
      "contractABI": contractABI,
      "byteCode": byteCode,
      "contractAddress": contractAddress,
      "accountAddress": accountAddress,
    };

    final result = await _methodChannel.invokeMethod(
        'getDynamicContractUserBalance', data);
    return result;
  }

  @override
  Future<String> getAccount() async {
    final result = await _methodChannel.invokeMethod('getAccount');
    return result;
  }

  @override
  Future<String> getUserBalance({required String accountAddress}) async {
    final result =
        await _methodChannel.invokeMethod('getUserBalance', accountAddress);
    return result;
  }

  @override
  Future<String> getCurrentGasPrice() async {
    final result =
        await _methodChannel.invokeMethod('getContractCurrentGasPrice');
    return result;
  }

  @override
  Future<bool> initializeMagic(
      {required String publisherKey, String? chainId, String? rpcUrl}) async {
    Map data = {
      "publisherKey": publisherKey,
      "chainId": chainId,
      "rpcUrl": rpcUrl
    };
    return await _methodChannel.invokeMethod('initializeMagic', data) as bool;
  }

  @override
  Future<String> sign(
      {required String fromAccount, required String message}) async {
    Map data = {
      "fromAccount": fromAccount,
      "message": message,
    };

    return await _methodChannel.invokeMethod('sign', data) as String;
  }

  @override
  Future<String> signTypedData({
    required String json,
    required String fromAccount,
  }) async {
    Map data = {
      "json": json,
      "fromAccount": fromAccount,
    };

    return await _methodChannel.invokeMethod('signTypedData', data) as String;
  }

  @override
  Future<String> signTypedDataLegacy(
      {required String fromAccount,
      required String type,
      required String name,
      required String value}) async {
    Map data = {
      "type": type,
      "name": name,
      "value": value,
      "fromAccount": fromAccount,
    };

    return await _methodChannel.invokeMethod('signTypedDataLegacy', data)
        as String;
  }
}
