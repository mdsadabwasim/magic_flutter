## 1.1.5

- Plugin deprecated in favor of the official magic plugin.

## 1.1.4

- Fixed minor issues

## 1.1.3

- Fixed issues of magic sdk

## 1.1.2

- Updated magic sdk version to 1.0.0

## 1.1.1

- Added getCurrentGasPrice method for contract

## 1.1.0

- Refactored code

## 1.0.9

- Added Sign method
- Added SignTypedData method
- Added signTypedDataLegacy (EIP 712) method

## 1.0.8

- Added null balance check

## 1.0.7

- Removed contract validity check

## 1.0.6

- Refactored code

## 1.0.5

- Refactored code

## 1.0.4

- passing bytecode from flutter side (only required on android)

## 1.0.3

- made user address to checksum converted

## 1.0.2

- Added support for custom `smart contract` with functionality of getting balance and sending transaction

## 1.0.1

- Added `showLoginUI` parameter in `loginWithMagicLink` function to show inbuilt log-in loading UI

## 1.0.0

- First release of the magic sdk plugin containing all the initial required parts to Login with email, Update email and Logout.
- It also contains functionality to check user balance, and send transaction.
