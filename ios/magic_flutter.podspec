#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint magic_flutter.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'magic_flutter'
  s.version          = '0.0.1'
  s.summary          = 'A new Flutter plugin for Magic SDK'
  s.description      = <<-DESC
A new Flutter plugin for Magic SDK
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.dependency 'MagicSDK'
  s.platform = :ios, '10.0'
  s.ios.deployment_target = '9.0'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = {
    'DEFINES_MODULE' => 'YES',
    'IPHONEOS_DEPLOYMENT_TARGET' => '9.0',
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386',
    'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }
  s.swift_version = '5.0'
end
