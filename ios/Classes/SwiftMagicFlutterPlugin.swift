import Flutter
import MagicSDK
import MagicSDK_Web3
import PromiseKit
import UIKit

enum EthereumConversionError: Error {
  case failedToConvert(String)
}

public class SwiftMagicFlutterPlugin: NSObject, FlutterPlugin {

  let etherInWei = pow(Decimal(10), 18)
  let etherInGwei = pow(Decimal(10), 9)

  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "magic", binaryMessenger: registrar.messenger())
    let instance = SwiftMagicFlutterPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {

    switch call.method {

    case "initializeMagic":
      self.initializeMagic(call: call, result: result)

    case "loginWithMagicLink":
      self.loginWithMagicLink(call: call, result: result)

    case "isLoggedIn":
      self.isLoggedIn(result: result)

    case "logout":
      self.logout(result: result)

    case "updateEmail":
      self.updateEmail(call: call, result: result)

    case "sendTransaction":
      self.sendTransaction(call: call, result: result)

    case "sendDynamicContractTransaction":
      self.sendDynamicContractTransaction(call: call, result: result)

    case "getDynamicContractUserBalance":
      self.getDynamicContractUserBalance(call: call, result: result)
        
    case "getContractCurrentGasPrice":
      self.getContractCurrentGasPrice(result: result)

    case "getAccount":
      self.getAccount(result: result)

    case "getUserBalance":
      self.getUserBalance(call: call, result: result)

    case "getMetaData":
      self.getMetaData(result: result)
    
    case "sign":
      self.sign(call: call, result: result)
    
    case "signTypedData":
      self.signTypedData(call: call, result: result)
    
    case "signTypedDataLegacy":
      self.signTypedDataLegacy(call: call, result: result)

    default:
      result(FlutterMethodNotImplemented)
    }

  }

  private func initializeMagic(call: FlutterMethodCall, result: @escaping FlutterResult) {

    let args = call.arguments as! [String: Any]

    if let publisherKey = args["publisherKey"] as? String,
      let rpcUrl = args["rpcUrl"] as? String,
      let chainId = args["chainId"] as? String
    {

      let config = CustomNodeConfiguration(
        rpcUrl: rpcUrl,
        chainId: Int(chainId))

      Magic.shared = Magic(apiKey: publisherKey, customNode: config)
      result(true)

    } else if let publisherKey = args["publisherKey"] as? String {
      Magic.shared = Magic(apiKey: publisherKey)
      result(true)
    } else {
      result(
        FlutterError(
          code: "INVALID_CALL",
          message: "Parameters unavailable",
          details: nil))
    }

  }

  private func loginWithMagicLink(call: FlutterMethodCall, result: @escaping FlutterResult) {

    let args = call.arguments as! [String: Any]

    if let email = args["email"] as? String {

      var config = LoginWithMagicLinkConfiguration(email: email)

      if let showUI = args["showUI"] as? Bool {
        config = LoginWithMagicLinkConfiguration(showUI: showUI, email: email)
      }

      Magic.shared.auth.loginWithMagicLink(config).done({ res in
        result(res)  // DIDToken
      }).catch({ error in
        result(
          FlutterError(
            code: "ERROR",
            message: error.localizedDescription,
            details: nil))  // handle Error
        print(error.localizedDescription)
      })

    } else {
      result(
        FlutterError(
          code: "INVALID_CALL",
          message: "Parameters unavailable",
          details: nil))
    }

  }

  private func isLoggedIn(result: @escaping FlutterResult) {

    Magic.shared.user.isLoggedIn(response: { response in
      guard let magicResult = response.result
      else {
        return result(false as Bool)
      }
      result(Bool(magicResult))
    })
  }

  private func getAccount(result: @escaping FlutterResult) {
    let web3 = Web3(provider: Magic.shared.rpcProvider)

    firstly {
      // Get user's Ethereum public address
      web3.eth.accounts()
    }.done { accounts -> Void in
      if let account = accounts.first {
        // Set to UILa
        result(try String(account.hex(eip55: true)))

      } else {
        print("No Account Found")
      }
    }.catch { error in
      result(
        FlutterError(
          code: "ERROR",
          message: "Error loading accounts and balance: \(error)",
          details: nil))
    }
  }

  private func sendTransaction(call: FlutterMethodCall, result: @escaping FlutterResult) {

    do {

      let args = call.arguments as! [String: Any]

      let amount = args["amount"] as! String
      //          let gasPrice = args["gasPrice"] as! String
      //          let gasLimit = args["gasLimit"] as! String
      let from = args["from"] as! String
      let to = args["to"] as! String

      let toAddress = try EthereumAddress(hex: to, eip55: true)

      let web3 = Web3(provider: Magic.shared.rpcProvider)

      let transaction = EthereumTransaction(
        from: try EthereumAddress(hex: from, eip55: true),  // from Get User Info section
        to: toAddress,
        value: EthereumQuantity(quantity: try self.toWei(ether: amount)))

      firstly {
        web3.eth.sendTransaction(transaction: transaction)
      }.done { transactionHash in
        result(try String(transactionHash.hex()))
      }.catch { error in
        result(
          FlutterError(
            code: "ERROR",
            message: "Error making transaction: \(error)",
            details: nil))
      }

    } catch let error {
      result(
        FlutterError(
          code: "ERROR",
          message: error.localizedDescription,
          details: nil))
    }
  }

  private func sendDynamicContractTransaction(call: FlutterMethodCall, result: @escaping FlutterResult) {
    do {
      let args = call.arguments as! [String: Any]
      let amount = args["amount"] as! String
      let _contractABI = args["contractABI"] as! String
      let _contractAddress = args["contractAddress"] as! String
      let gasPrice = args["gasPrice"] as! String
      let gasLimit = args["gasLimit"] as! String
      let from = args["from"] as! String
      let to = args["to"] as! String

      let web3 = Web3(provider: Magic.shared.rpcProvider)

      let contractABI = _contractABI.data(using: .utf8)!
      let contractAddress = try EthereumAddress(
        hex: _contractAddress, eip55: true)
      let contract = try web3.eth.Contract(json: contractABI, abiKey: nil, address: contractAddress)
      let toAddress = try EthereumAddress(hex: to, eip55: true)
      let fromAddress = try EthereumAddress(hex: from, eip55: true)

      firstly {
        web3.eth.getTransactionCount(address: fromAddress, block: .pending)
      }.done { nonce in

        guard
          let transaction = contract["transfer"]?(toAddress, try self.toWei(ether: amount))
            .createTransaction(
              nonce: nonce,
              from: fromAddress,
              value: 0,
              gas: EthereumQuantity(quantity: BigUInt(gasLimit, radix: 10)!),
              gasPrice: EthereumQuantity(quantity: BigUInt(gasPrice, radix: 10)!)
            )
        else { return }

        web3.eth.sendTransaction(transaction: transaction).done({ txHash in
          result(try String(txHash.hex()))
        }).catch { error in
          print(error.localizedDescription)
          result(
            FlutterError(
              code: "ERROR",
              message: error.localizedDescription,
              details: nil))

        }

      }.catch { error in
        print(error.localizedDescription)
        result(
          FlutterError(
            code: "ERROR",
            message: error.localizedDescription,
            details: nil))
      }

    } catch let error {

      print(error.localizedDescription)
      result(
        FlutterError(
          code: "ERROR",
          message: error.localizedDescription,
          details: nil))
    }
  }

  private func getDynamicContractUserBalance(
    call: FlutterMethodCall, result: @escaping FlutterResult
  ) {
    do {
      let args = call.arguments as! [String: Any]
      let _contractABI = args["contractABI"] as! String
      let _contractAddress = args["contractAddress"] as! String
      let accountAddress = args["accountAddress"] as! String

      let web3 = Web3(provider: Magic.shared.rpcProvider)

      let contractABI = _contractABI.data(using: .utf8)!
      let contractAddress = try EthereumAddress(
        hex: _contractAddress, eip55: true)
      let contract = try web3.eth.Contract(json: contractABI, abiKey: nil, address: contractAddress)
      let address = try EthereumAddress(hex: accountAddress, eip55: true)

      contract["balanceOf"]?(address).call(block: .latest) { response, error in
        if let response = response, let balance = response[""] as? BigUInt {
          result(self.toEther(wei: balance).description)
        } else {
          result(
            FlutterError(
              code: "ERROR",
              message: error?.localizedDescription ?? "Empty response",
              details: nil))
        }
      }

    } catch let error {

      print(error.localizedDescription)
      result(
        FlutterError(
          code: "ERROR",
          message: error.localizedDescription,
          details: nil))
    }
  }

  private func getMetaData(result: @escaping FlutterResult) {
    // Assuming user is logged in
    Magic.shared.user.getMetadata(response: { response in
      guard
        let metadata = response.result
      else {
        result(
          FlutterError(
            code: "ERROR",
            message: response.error!.localizedDescription,
            details: nil))
        return
      }

      let meta: [String: Any] = [
        "issuer": metadata.issuer!,
        "publicAddress": metadata.publicAddress!,
        "email": metadata.email!,
      ]

      result(meta as [String: Any])
    })
  }

  private func updateEmail(call: FlutterMethodCall, result: @escaping FlutterResult) {

    if let newEmail = call.arguments as? String {
      // Assuming user is logged in
      let configuration = UpdateEmailConfiguration(email: newEmail)

      Magic.shared.user.updateEmail(
        configuration,
        response: { response in
          guard
            let magicResult = response.result
          else {
            return result(
              FlutterError(
                code: "ERROR",
                message: response.error!.localizedDescription,
                details: nil))
          }

          result(Bool(magicResult))

        })
    } else {
      result(
        FlutterError(
          code: "INVALID_CALL",
          message: "Parameters unavailable",
          details: nil))
    }

  }

  private func logout(result: @escaping FlutterResult) {
    // Assuming user is logged in
    Magic.shared.user.logout(response: { response in
      guard
        let magicResult = response.result
      else {
        return result(
          FlutterError(
            code: "ERROR",
            message: response.error!.localizedDescription,
            details: nil))
      }
      result(Bool(magicResult))
    })
  }

  private func getUserBalance(call: FlutterMethodCall, result: @escaping FlutterResult) {

    if let address = call.arguments as? String {

      let web3 = Web3(provider: Magic.shared.rpcProvider)

      let e = try? EthereumAddress(hex: address, eip55: false)

      guard let ethereumAddress = e else {
        return
      }

      firstly {
        web3.eth.getBalance(address: ethereumAddress, block: .latest)

      }.done { ethereumQuantity in

        let eth: Decimal = self.toEther(wei: ethereumQuantity.quantity)

        result(eth.description)
      }.catch { error in
        result(
          FlutterError(
            code: "ERROR",
            message: error.localizedDescription,
            details: nil))
      }

    }

  }

    private func getContractCurrentGasPrice(result: @escaping FlutterResult) {

        let web3 = Web3(provider: Magic.shared.rpcProvider)

        
        firstly {
            web3.eth.gasPrice()

        }.done { ethereumQuantity in

            result(ethereumQuantity.quantity.description)
        }.catch { error in
          result(
            FlutterError(
              code: "ERROR",
              message: error.localizedDescription,
              details: nil))
        }
    }

  private func sign(call: FlutterMethodCall, result: @escaping FlutterResult) {

    do {

      let args = call.arguments as! [String: Any]

      let _fromAccount = args["fromAccount"] as! String

      let _message = args["message"] as! String

      let web3 = Web3(provider: Magic.shared.rpcProvider)

      let message = try! EthereumData(_message.data(using: .utf8)!)
      let fromAccount = try EthereumAddress(hex: _fromAccount, eip55: true)

      firstly {
        web3.eth.sign(from: fromAccount, message: message)

      }.done { transactionHash in
        result(try String(transactionHash.hex()))
      }.catch { error in
        result(
          FlutterError(
            code: "ERROR",
            message: "Error signing message: \(error)",
            details: nil))
      }

    } catch let error {
      result(
        FlutterError(
          code: "ERROR",
          message: error.localizedDescription,
          details: nil))
    }
  }

  private func signTypedData(call: FlutterMethodCall, result: @escaping FlutterResult) {

    do {

      let args = call.arguments as! [String: Any]

      let _fromAccount = args["fromAccount"] as! String

      let json = args["json"] as! String

      let web3 = Web3(provider: Magic.shared.rpcProvider)

      let typedData = try JSONDecoder().decode(EIP712TypedData.self, from: json.data(using: .utf8)!)

      let fromAccount = try EthereumAddress(hex: _fromAccount, eip55: true)

      firstly {
        web3.eth.signTypedData(account: fromAccount, data: typedData)


      }.done { transactionHash in
        result(try String(transactionHash.hex()))
      }.catch { error in
        result(
          FlutterError(
            code: "ERROR",
            message: "Error signing message: \(error)",
            details: nil))
      }

    } catch let error {
      result(
        FlutterError(
          code: "ERROR",
          message: error.localizedDescription,
          details: nil))
    }
  }

  private func signTypedDataLegacy(call: FlutterMethodCall, result: @escaping FlutterResult) {

    do {

      let args = call.arguments as! [String: Any]

      let _fromAccount = args["fromAccount"] as! String
      let type = args["type"] as! String

      let name = args["name"] as! String
      let value = args["value"] as! String

      let web3 = Web3(provider: Magic.shared.rpcProvider)

      let payload = EIP712TypedDataLegacyFields(
        type: type, name: name, value: value)

      let fromAccount = try EthereumAddress(hex: _fromAccount, eip55: true)

      firstly {
        web3.eth.signTypedDataLegacy(account: fromAccount, data: [payload])

      }.done { transactionHash in
        result(try String(transactionHash.hex()))
      }.catch { error in
        result(
          FlutterError(
            code: "ERROR",
            message: "Error signing message: \(error)",
            details: nil))
      }

    } catch let error {
      result(
        FlutterError(
          code: "ERROR",
          message: error.localizedDescription,
          details: nil))
    }
  }

  private func toEther(wei: BigUInt) -> Decimal {

    guard let decimalWei = Decimal(string: wei.description) else {
      return 0
    }

    return decimalWei / etherInWei

  }

  private func toWei(ether: Decimal) throws -> BigUInt {
    guard let wei = BigUInt((ether * etherInWei).description) else {
      throw EthereumConversionError.failedToConvert("Failed to convert eth -> wei")
    }
    return wei
  }

  private func toGwei(ether: Decimal) throws -> BigUInt {
    guard let gwei = BigUInt((ether * etherInGwei).description) else {
      throw EthereumConversionError.failedToConvert("Failed to convert eth -> gwei")
    }
    return gwei
  }

  private func toGwei(ether: String) throws -> BigUInt {
    guard let decimalEther = Decimal(string: ether) else {
      throw EthereumConversionError.failedToConvert("Failed to convert eth -> gwei")
    }
    return try toGwei(ether: decimalEther)
  }

  private func toWei(ether: String) throws -> BigUInt {
    guard let decimalEther = Decimal(string: ether) else {
      throw EthereumConversionError.failedToConvert("Failed to convert eth -> wei")
    }
    return try toWei(ether: decimalEther)
  }

}
