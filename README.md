# magic_flutter
<a href="https://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/license-MIT-purple.svg" alt="License: MIT"></a>
<a href="https://gitlab.com/mdsadabwasim/magic_flutter"><img src="https://img.shields.io/badge/platform-flutter-ff69b4.svg" alt="Flutter Platform Badge"></a>

This plugin is deprecated in favor of [official magic plugin](https://pub.dev/packages/magic_sdk), it is recommended to shift to the official plugin.

This is a specialized package that includes platform-specific implementation code for
Android and iOS side [Magic sdk](https://magic.link/) implementation.

## Features implemented

- Login with Magic sdk
- Logout functionality
- Update email 
- Check User's balance
- Send BNB to other user
- Get user's account address
- Work with custom smart contracts


## How to use

- First of all get your publisher key from [Magic sdk](https://magic.link/) And `initialize magic sdk`

```
FutureBuilder(
        future: Magic.initializeMagic(
            publisherKey: //your key here
            ),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
             return Container(
                alignment: Alignment.center,
                child: Text(
                    "Successfully initialized Magic"),
              );
            } else {
              return Container(
                alignment: Alignment.center,
                child: Text("Something went wrong. Failed to initialize Magic"),
              );
            }
          } else
            return Center(child: CircularProgressIndicator());
        },
      )
```
- Once you have intialized sdk you can login with email, you have to also provide `showLoadingUI` parameter pass `true` to show out of the box magic sdk loading UI and `false` to show your custom loading UI

```
 await Magic.loginWithMagicLink(email: "test@gmail.com", showLoadingUI: true);
```

- We can logout using logout function which returns a boolean that indicate successfull operation

```
bool value = await Magic.logout();
```

- We can also check if user is already Logged in or not, it also returns a bool value that indicated if user is logged in.

```
bool value = await Magic.isLoggedIn();
```

- After login we can get meta data, which return GetMetaDataResponse that contains three values
`email, issuer and publicAddress`

```
FutureBuilder<GetMetaDataResponse>(
        future: Magic.getMetaData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
             return Container(
                alignment: Alignment.center,
                child: Text(snapshot.data!.email),
              );
            } else {
              return Container(
                alignment: Alignment.center,
                child: Text("Something went wrong. Failed to get metadata"),
              );
            }
          } else
            return Center(child: CircularProgressIndicator());
        },
      )
```


# Blockchain related methods

If you want to do blockchain transactions follow this section

### First of all intialize Magic sdk with `CustomNodeConfiguration`

```
Magic.initializeMagic(
            publisherKey: publisherKey,
            rpcURL: "https://data-seed-prebsc-1-s1.binance.org:8545/",   // Smart Chain - Testnet RPC URL
            chainID: "97" // Smart Chain - Testnet chain 
            )
```

- Once initialized you can Login and get meta data (need to get user account address)

### Check user balance, it returns balance in BNB `[String]`

```
 String value = await Magic.getUserBalance(
     accountAddress: //Enter user account address here);
```

## Send transaction

### For Android

- You have to pass these values with `amount` in BNB `[String]`, it will return transaction hash on successfull transaction.
```
String value = await Magic.sendTransaction(
          from://your account address
          to: //receiver address,
          amount://amount you want to send
          gasLimit://gas limit,
          gasPrice://gas price);
```

### For IOS

- You have to pass only `from, to and amount` parameter, it will return transaction hash on successfull transaction.
```
String value = await Magic.sendTransaction(
          from://your account address
          to: //receiver address,
          amount://amount you want to send);
```

# Working with smart contract


### For IOS

 - You need to have your deployed contract abi.json file and contract address.

### For Android (Not directly supported)

 - You need to have your deployed contract abi.json, bytecode.bin files and contract address.
 - Unfortunately for android side we need to create a custom java wrapper class using web3j cli and put it in plugin project to interact with contract.
 - If you need to work on android side, kindly fork the plugin project in Gitlab and replace `GenericContract.java` with your generated java file.


### Check user balance with custom contract 
- it returns balance `[String]`

```
 String value = await Magic.getDynamicContractUserBalance(
     accountAddress://Enter user account address here,
      contractABI://Contract abi in form of string,
      byteCode://contract bytecode in form of string
      contractAddress://Deployed contract address
     );
```

### Send transaction with custom contract


- You have to pass these values with `amount` in `[String]`, it will return transaction hash on successfull transaction.
```
String value = await Magic.sendDynamicContractTransaction(
          from://your account address
          to://receiver address,
          amount://amount you want to send,
          contractABI://contract abi in form of string
          byteCode://contract bytecode in form of string
          contractAddress://your deployed contract address
          gasLimit://gas limit,
          gasPrice://gas price);
```
















